﻿using System.Web;
using System.Web.Mvc;

namespace Recreators_API_Microservice
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
