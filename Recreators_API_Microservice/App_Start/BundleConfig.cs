﻿using System.Web;
using System.Web.Optimization;

namespace Recreators_API_Microservice
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap"));

            bundles.Add(new StyleBundle("~/Content/css"));
        }
    }
}
