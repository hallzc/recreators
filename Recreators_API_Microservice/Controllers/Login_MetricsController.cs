﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class Login_MetricsController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        [HttpGet]
        public ActionResult LoginMetrics()
        {
            var loginMericsList = db.Login_Metrics.Include(l => l.User_Metrics).ToList();

            string loginMetricsString = "";
            int c = 0;

            foreach(var loginMetric in loginMericsList)
            {
                if (c > 0)
                {
                    loginMetricsString += mainDelim + loginMetric.User_ID + subDelim + loginMetric.Last_Login + subDelim + loginMetric.Last_Logout;
                    c++;
                }
                else
                {
                    loginMetricsString += loginMetric.User_ID + subDelim + loginMetric.Last_Login + subDelim + loginMetric.Last_Logout;
                    c++;
                }
            }

            return Content(loginMetricsString);
        }

        // GET: Login_Metrics
        public ActionResult Index()
        {
            var login_Metrics = db.Login_Metrics.Include(l => l.User_Metrics);
            return View(login_Metrics.ToList());
        }

        // GET: Login_Metrics/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Login_Metrics login_Metrics = db.Login_Metrics.Find(id);
            if (login_Metrics == null)
            {
                return HttpNotFound();
            }
            return View(login_Metrics);
        }

        [HttpGet]
        public ActionResult LoginMetricDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Login_Metrics loginMetric = db.Login_Metrics.Find(id);
            string loginMetricString = loginMetric.User_Metrics + subDelim + loginMetric.Last_Login + subDelim + loginMetric.Last_Logout;
            if (loginMetric == null)
            {
                return HttpNotFound();
            }
           
            return Content(loginMetricString);
        }

        // GET: Login_Metrics/Create
        public ActionResult Create()
        {
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username");
            return View();
        }

        // POST: Login_Metrics/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Login_ID,User_ID,Last_Login,Last_Logout")] Login_Metrics login_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Login_Metrics.Add(login_Metrics);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", login_Metrics.User_ID);
            return View(login_Metrics);
        }

        [HttpPost]
        public void LoginMetricCreate([Bind(Include = "Login_ID,User_ID,Last_Login,Last_Logout")] Login_Metrics login_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Login_Metrics.Add(login_Metrics);
                db.SaveChanges();               
            }
        }

        // GET: Login_Metrics/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Login_Metrics login_Metrics = db.Login_Metrics.Find(id);
            if (login_Metrics == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", login_Metrics.User_ID);
            return View(login_Metrics);
        }

        // POST: Login_Metrics/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Login_ID,User_ID,Last_Login,Last_Logout")] Login_Metrics login_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(login_Metrics).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", login_Metrics.User_ID);
            return View(login_Metrics);
        }

        [HttpPost]
        public void LoginMetricEdit([Bind(Include = "Login_ID,User_ID,Last_Login,Last_Logout")] Login_Metrics login_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(login_Metrics).State = EntityState.Modified;
                db.SaveChanges();
                
            }
        }


        // GET: Login_Metrics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Login_Metrics login_Metrics = db.Login_Metrics.Find(id);
            if (login_Metrics == null)
            {
                return HttpNotFound();
            }
            return View(login_Metrics);
        }

        // POST: Login_Metrics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Login_Metrics login_Metrics = db.Login_Metrics.Find(id);
            db.Login_Metrics.Remove(login_Metrics);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpDelete]
        public void LoginMetricDeleleConfirmed(int id)
        {
            Login_Metrics login_Metrics = db.Login_Metrics.Find(id);
            db.Login_Metrics.Remove(login_Metrics);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
