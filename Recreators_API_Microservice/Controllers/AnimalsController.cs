﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class AnimalsController : Controller 
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        [HttpGet]
        // GET: Animals
        public ActionResult Animals()
        {
            var animalList = db.Animals.ToList();
       
            string animalString = "";
            int c = 0;
           
            foreach (var animal in animalList)
            {
                if(c > 0)
                {
                    animalString += mainDelim + animal.Name + subDelim + animal.Breed + subDelim + animal.Category + subDelim + animal.Date_Of_Birth + subDelim + animal.Registered + subDelim + animal.Color;
                    c++;
                }
                else
                {
                    animalString += animal.Name + subDelim + animal.Breed + subDelim + animal.Category + subDelim + animal.Date_Of_Birth + subDelim + animal.Registered + subDelim + animal.Color;
                    c++;
                }
                
            }
            return Content(animalString);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(db.Animals.ToList());
        }

        // GET: Animals/Details/5
        [HttpGet]
        public ActionResult AnimalDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animals.Find(id);
            string animalString = animal.Name + subDelim + animal.Breed + subDelim + animal.Category + subDelim + animal.Date_Of_Birth + subDelim + animal.Registered + subDelim + animal.Color;
            if (animal == null)
            {
                return HttpNotFound();
            }
            return Content(animalString);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animals.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // GET: Animals/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Animals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Animal_ID,Name,Breed,Category,Date_Of_Birth,Registered,Color")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                db.Animals.Add(animal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(animal);
        }

        [HttpPost]
        public void AnimalCreate([Bind(Include = "Animal_ID,Name,Breed,Category,Date_Of_Birth,Registered,Color")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                db.Animals.Add(animal);
                db.SaveChanges();
               
            }

         
        }

        // GET: Animals/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animals.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // POST: Animals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Animal_ID,Name,Breed,Category,Date_Of_Birth,Registered,Color")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(animal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(animal);
        }

        [HttpPost]
        public void AnimalEdit([Bind(Include = "Animal_ID,Name,Breed,Category,Date_Of_Birth,Registered,Color")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(animal).State = EntityState.Modified;
                db.SaveChanges();
              
            }
            
        }

        // GET: Animals/Delete/5
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Animal animal = db.Animals.Find(id);
            if (animal == null)
            {
                return HttpNotFound();
            }
            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpDelete, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Animal animal = db.Animals.Find(id);
            db.Animals.Remove(animal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

   
        [HttpDelete, ActionName("AnimalDeleteConfirmed")]
        public void AnimalDeleteConfirmed(int id)
        {
            Animal animal = db.Animals.Find(id);
            db.Animals.Remove(animal);
            db.SaveChanges();  
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
