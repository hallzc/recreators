﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class CartsController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        // GET: Carts
        [HttpGet]
        public ActionResult Carts()
        {
            var cartsList = db.Carts.ToList();
            //Cart_ID,User_ID,Qty,Item_Added_Time,Cart_Close_Time,Cart_Close_Type
            string cartsString = "";
            int c = 0;

            foreach (var item in cartsList)
            {
                if (c > 0)
                {
                    cartsString += mainDelim + item.Cart_ID + subDelim + item.User_ID + subDelim + item.Qty + subDelim + item.Item_Added_Time + subDelim + item.Cart_Close_Time + subDelim + item.Cart_Close_Type;
                    c++;
                }
                else
                {
                    cartsString += item.Cart_ID + subDelim + item.User_ID + subDelim + item.Qty + subDelim + item.Item_Added_Time + subDelim + item.Cart_Close_Time + subDelim + item.Cart_Close_Type;
                    c++;
                }

            }
            return Content(cartsString);
        }
        public ActionResult Index()
        {
            var carts = db.Carts.Include(c => c.User_Metrics);
            return View(carts.ToList());
        }

        // GET: Carts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
           
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        [HttpGet]
        public ActionResult CartDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart carts = db.Carts.Find(id);

            if (carts == null)
            {
                return HttpNotFound();
            }
            string cartsString = carts.Cart_ID + subDelim + carts.User_ID + subDelim + carts.Qty + subDelim + carts.Item_Added_Time + subDelim + carts.Cart_Close_Time + subDelim + carts.Cart_Close_Type;

            return Content(cartsString);
        }

        // GET: Carts/Create
        public ActionResult Create()
        {
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username");
            return View();
        }

        // POST: Carts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Cart_ID,User_ID,Qty,Item_Added_Time,Cart_Close_Time,Cart_Close_Type")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                db.Carts.Add(cart);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", cart.User_ID);
            return View(cart);
        }

        [HttpPost]
        public void CartsCreate([Bind(Include = "Cart_ID,User_ID,Qty,Item_Added_Time,Cart_Close_Time,Cart_Close_Type")] Cart carts)
        {
            if (ModelState.IsValid)
            {
                db.Carts.Add(carts);
                db.SaveChanges();

            }


        }
        // GET: Carts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", cart.User_ID);
            return View(cart);
        }

        // POST: Carts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Cart_ID,User_ID,Qty,Item_Added_Time,Cart_Close_Time,Cart_Close_Type")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cart).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", cart.User_ID);
            return View(cart);
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public void CartsEdit([Bind(Include = "Cart_ID,User_ID,Qty,Item_Added_Time,Cart_Close_Time,Cart_Close_Type")] Cart cart)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cart).State = EntityState.Modified;
                db.SaveChanges();
                
            }
           
        }
        // GET: Carts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart cart = db.Carts.Find(id);
            if (cart == null)
            {
                return HttpNotFound();
            }
            return View(cart);
        }

        // POST: Carts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cart cart = db.Carts.Find(id);
            db.Carts.Remove(cart);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Animals/Delete/5
        [HttpDelete, ActionName("CartsDeleteConfirmed")]
        //[ValidateAntiForgeryToken]
        public void CartsDeleteConfirmed(int id)
        {
            Cart carts = db.Carts.Find(id);
            db.Carts.Remove(carts);
            db.SaveChanges();

            //return Json("Item has been deleted.", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
