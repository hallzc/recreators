﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class Cart_MerchandiseController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        // GET: Cart_Merchandise
        [HttpGet]
        public ActionResult CartMerchandise()
        {
            var cartList = db.Cart_Merchandise.ToList();

            string cartString = "";
            int c = 0;

            foreach (var item in cartList)
            {
                if (c > 0)
                {
                    cartString += mainDelim + item.Cart_ID + subDelim + item.Merchandise.Name + subDelim + item.Merchandise.Description;
                    c++;
                }
                else
                {
                    cartString += item.Cart_ID + subDelim + item.Merchandise.Name + subDelim + item.Merchandise.Description;
                    c++;
                }

            }
            return Content(cartString);
        }
        public ActionResult Index()
        {
            var cart_Merchandise = db.Cart_Merchandise.Include(c => c.Merchandise);
            return View(cart_Merchandise.ToList());
        }

        // GET: Cart_Merchandise/Details/5
        [HttpGet]
        public ActionResult CartDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            string cartString = cart_Merchandise.Cart_ID + subDelim + cart_Merchandise.Merchandise;
            if (cart_Merchandise == null)
            {
                return HttpNotFound();
            }
            return Content(cartString);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            if (cart_Merchandise == null)
            {
                return HttpNotFound();
            }
            return View(cart_Merchandise);
        }


        // GET: Cart_Merchandise/Create
        public ActionResult Create()
        {
            ViewBag.Merch_ID = new SelectList(db.Merchandises, "Merch_ID", "Name");
            return View();
        }

        // POST: Cart_Merchandise/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Cart_ID,Merch_ID")] Cart_Merchandise cart_Merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Cart_Merchandise.Add(cart_Merchandise);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Merch_ID = new SelectList(db.Merchandises, "Merch_ID", "Name", cart_Merchandise.Merch_ID);
            return View(cart_Merchandise);
        }


        [HttpPost]
        public void CartCreate([Bind(Include = "Merch_ID,Cart_ID")] Cart_Merchandise cart_Merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Cart_Merchandise.Add(cart_Merchandise);
                db.SaveChanges();

            }


        }



        // GET: Cart_Merchandise/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            if (cart_Merchandise == null)
            {
                return HttpNotFound();
            }
            ViewBag.Merch_ID = new SelectList(db.Merchandises, "Merch_ID", "Name", cart_Merchandise.Merch_ID);
            return View(cart_Merchandise);
        }

        // POST: Cart_Merchandise/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Cart_ID,Merch_ID")] Cart_Merchandise cart_Merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cart_Merchandise).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Merch_ID = new SelectList(db.Merchandises, "Merch_ID", "Name", cart_Merchandise.Merch_ID);
            return View(cart_Merchandise);
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public void CartEdit([Bind(Include = "Cart_ID,Merch_ID")] Cart_Merchandise cart_Merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cart_Merchandise).State = EntityState.Modified;
                db.SaveChanges();
                // return RedirectToAction("Index");
            }
            //return Json(animal, JsonRequestBehavior.AllowGet);
        }


        // GET: Cart_Merchandise/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            if (cart_Merchandise == null)
            {
                return HttpNotFound();
            }
            return View(cart_Merchandise);
        }

        // POST: Cart_Merchandise/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            db.Cart_Merchandise.Remove(cart_Merchandise);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Cart/Delete/5
        [HttpDelete, ActionName("CartDeleteConfirmed")]
        //[ValidateAntiForgeryToken]
        public void CartDeleteConfirmed(int id)
        {
            Cart_Merchandise cart_Merchandise = db.Cart_Merchandise.Find(id);
            db.Cart_Merchandise.Remove(cart_Merchandise);
            db.SaveChanges();

            //return Json("Item has been deleted.", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
