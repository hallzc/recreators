﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class AdoptionsController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        // GET: adoptions/adoptions
        [HttpGet]
        public ActionResult Adoptions()
        {
            var adoptionList = db.Adoptions.Include(a => a.Animal).Include(a => a.User_Metrics).ToList();

            string adoptionString = "";
            int c = 0;

            foreach (var adoption in adoptionList)
            {
                if (c > 0)
                {
                    adoptionString += mainDelim  + adoption.User_ID + subDelim + adoption + subDelim + adoption.Adoption_Complete + 
                        subDelim + adoption.Animal.Breed + subDelim +adoption.Animal.Category + subDelim + adoption.Animal.Registered + subDelim + adoption.Animal.Color + subDelim + adoption.Animal.Date_Of_Birth;
                    c++;
                }
                else
                {
                    adoptionString += adoption.User_ID + subDelim + adoption.Adoption_Time + subDelim + adoption.Adoption_Complete +
                        subDelim + adoption.Animal.Breed + subDelim + adoption.Animal.Category + subDelim + adoption.Animal.Registered + subDelim + adoption.Animal.Color + subDelim + adoption.Animal.Date_Of_Birth;
                    c++;
                }

            }
            return Content(adoptionString);


       
        }

        // GET: Adoptions
        public ActionResult Index()
        {
            var adoptions = db.Adoptions.Include(a => a.Animal).Include(a => a.User_Metrics);
            return View(adoptions.ToList());
        }

        // GET: Adoptions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            return View(adoption);
        }
        [HttpGet]
        public ActionResult AdotionDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id);
            string adoptionString = adoption.User_ID + subDelim + adoption.Adoption_Time + subDelim + adoption.Adoption_Complete +
                                    subDelim + adoption.Animal.Breed + subDelim + adoption.Animal.Category + subDelim + adoption.Animal.Registered + subDelim + adoption.Animal.Color + subDelim + adoption.Animal.Date_Of_Birth; 
            if (adoption == null)
            {
                return HttpNotFound();
            }
            
            return Content(adoptionString);
        }

        // GET: Adoptions/Create
        public ActionResult Create()
        {
            ViewBag.Animal_ID = new SelectList(db.Animals, "Animal_ID", "Name");
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username");
            return View();
        }

      

        // POST: Adoptions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Adoption_ID,User_ID,Animal_ID,Adoption_Time,Adoption_Complete")] Adoption adoption)
        {
            if (ModelState.IsValid)
            {
                db.Adoptions.Add(adoption);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Animal_ID = new SelectList(db.Animals, "Animal_ID", "Name", adoption.Animal_ID);
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", adoption.User_ID);
            return View(adoption);
        }

        [HttpPost]
        public void AdoptionCreate([Bind(Include = "Adoption_ID,User_ID,Animal_ID,Adoption_Time,Adoption_Complete")] Adoption adoption)
        {
            if(ModelState.IsValid)
            {
                db.Adoptions.Add(adoption);
                db.SaveChanges();
            
            }
        }

        // GET: Adoptions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            ViewBag.Animal_ID = new SelectList(db.Animals, "Animal_ID", "Name", adoption.Animal_ID);
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", adoption.User_ID);
            return View(adoption);
        }

        // POST: Adoptions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Adoption_ID,User_ID,Animal_ID,Adoption_Time,Adoption_Complete")] Adoption adoption)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adoption).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Animal_ID = new SelectList(db.Animals, "Animal_ID", "Name", adoption.Animal_ID);
            ViewBag.User_ID = new SelectList(db.User_Metrics, "User_ID", "Username", adoption.User_ID);
            return View(adoption);
        }

        [HttpPost]
        public void AdoptionEdit([Bind(Include = "Adoption_ID,User_ID,Animal_ID,Adoption_Time,Adoption_Complete")] Adoption adoption)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adoption).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        // GET: Adoptions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adoption adoption = db.Adoptions.Find(id);
            if (adoption == null)
            {
                return HttpNotFound();
            }
            return View(adoption);
        }

        // POST: Adoptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Adoption adoption = db.Adoptions.Find(id);
            db.Adoptions.Remove(adoption);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpDelete]
        public void AdoptionDeleteConfirmed(int id)
        {
            Adoption adoption = db.Adoptions.Find(id);
            db.Adoptions.Remove(adoption);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
