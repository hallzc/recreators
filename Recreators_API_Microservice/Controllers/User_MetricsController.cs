﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class User_MetricsController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        [HttpGet]
        public ActionResult UserMetrics()
        {
            var userMericsList = db.User_Metrics.ToList();

            string userMetricsString = "";
            int c = 0;

            foreach (var userMetric in userMericsList)
            {
                if (c > 0)
                {
                    userMetricsString += mainDelim + userMetric.Username + subDelim + userMetric.First_Name + subDelim + userMetric.Last_Name + subDelim + userMetric.Email 
                                            + subDelim + userMetric.Sex + subDelim + userMetric.Race + subDelim + userMetric.City + subDelim + userMetric.State + subDelim + userMetric.Date_Of_Birth;
                    c++;
                }
                else
                {
                    userMetricsString += userMetric.Username + subDelim + userMetric.First_Name + subDelim + userMetric.Last_Name + subDelim + userMetric.Email
                                            + subDelim + userMetric.Sex + subDelim + userMetric.Race + subDelim + userMetric.City + subDelim + userMetric.State + subDelim + userMetric.Date_Of_Birth;
                    c++;
                }
            }
            return Content(userMetricsString);
        }

        // GET: User_Metrics
        public ActionResult Index()
        {
            return View(db.User_Metrics.ToList());
        }

        // GET: User_Metrics/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User_Metrics user_Metrics = db.User_Metrics.Find(id);
            if (user_Metrics == null)
            {
                return HttpNotFound();
            }
            return View(user_Metrics);
        }

        [HttpGet]
        public ActionResult UserMetricDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User_Metrics userMetric = db.User_Metrics.Find(id);
            string userMetricString = userMetric.Username + subDelim + userMetric.First_Name + subDelim + userMetric.Last_Name + subDelim + userMetric.Email
                                            + subDelim + userMetric.Sex + subDelim + userMetric.Race + subDelim + userMetric.City + subDelim + userMetric.State + subDelim + userMetric.Date_Of_Birth;
            if (userMetric == null)
            {
                return HttpNotFound();
            }
            return Content(userMetricString);
        }

        // GET: User_Metrics/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User_Metrics/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "User_ID,Username,First_Name,Last_Name,Email,Sex,Race,City,State,Date_Of_Birth")] User_Metrics user_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.User_Metrics.Add(user_Metrics);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user_Metrics);
        }

        [HttpPost]
        public void UserMetricCreate([Bind(Include = "User_ID,Username,First_Name,Last_Name,Email,Sex,Race,City,State,Date_Of_Birth")] User_Metrics user_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.User_Metrics.Add(user_Metrics);
                db.SaveChanges();                
            }
        }

        // GET: User_Metrics/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User_Metrics user_Metrics = db.User_Metrics.Find(id);
            if (user_Metrics == null)
            {
                return HttpNotFound();
            }
            return View(user_Metrics);
        }

        // POST: User_Metrics/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "User_ID,Username,First_Name,Last_Name,Email,Sex,Race,City,State,Date_Of_Birth")] User_Metrics user_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user_Metrics).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user_Metrics);
        }

        [HttpPost]
        public void UserMetricEdit([Bind(Include = "User_ID,Username,First_Name,Last_Name,Email,Sex,Race,City,State,Date_Of_Birth")] User_Metrics user_Metrics)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user_Metrics).State = EntityState.Modified;
                db.SaveChanges();               
            }
        }

        // GET: User_Metrics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User_Metrics user_Metrics = db.User_Metrics.Find(id);
            if (user_Metrics == null)
            {
                return HttpNotFound();
            }
            return View(user_Metrics);
        }

        // POST: User_Metrics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User_Metrics user_Metrics = db.User_Metrics.Find(id);
            db.User_Metrics.Remove(user_Metrics);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpDelete]
        public void UserMetricDelete(int id)
        {
            User_Metrics user_Metrics = db.User_Metrics.Find(id);
            db.User_Metrics.Remove(user_Metrics);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
