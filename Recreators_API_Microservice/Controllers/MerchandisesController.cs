﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers
{
    public class MerchandisesController : Controller
    {
        private ApiModel db = new ApiModel();
        string mainDelim = ";";
        string subDelim = ",";

        // GET: Merchandises
        [HttpGet]
        public ActionResult Merchandises()
        {
            var merchList = db.Merchandises.ToList();
       
            string merchString = "";
            int c = 0;

            foreach (var item in merchList)
            {
                if (c > 0)
                {
                    merchString += mainDelim + item.Merch_ID + subDelim + item.Name + subDelim + item.Description;
                    c++;
                }
                else
                {
                    merchString += item.Merch_ID + subDelim + item.Name + subDelim + item.Description;
                    c++;
                }

            }
            return Content(merchString);
        }
        public ActionResult Index()
        {
            return View(db.Merchandises.ToList());
        }

        // GET: Merchandises/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Merchandise merchandise = db.Merchandises.Find(id);
            if (merchandise == null)
            {
                return HttpNotFound();
            }
            return View(merchandise);
        }

        public ActionResult MerchandisesDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Merchandise merchandise = db.Merchandises.Find(id);
            string merchString = merchandise.Merch_ID + subDelim + merchandise.Name + subDelim + merchandise.Description;
            if (merchandise == null)
            {
                return HttpNotFound();
            }
            return Content(merchString);
        }



        // GET: Merchandises/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Merchandises/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Merch_ID,Name,Description")] Merchandise merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Merchandises.Add(merchandise);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(merchandise);
        }

        [HttpPost]
        public void MerchCreate([Bind(Include = "Merch_ID,Name,Description")] Merchandise merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Merchandises.Add(merchandise);
                db.SaveChanges();

            }


        }

        // GET: Merchandises/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Merchandise merchandise = db.Merchandises.Find(id);
            if (merchandise == null)
            {
                return HttpNotFound();
            }
            return View(merchandise);
        }

        // POST: Merchandises/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Merch_ID,Name,Description")] Merchandise merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Entry(merchandise).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(merchandise);
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public void MerchEdit([Bind(Include = "Merch_ID,Name,Description")] Merchandise merchandise)
        {
            if (ModelState.IsValid)
            {
                db.Entry(merchandise).State = EntityState.Modified;
                db.SaveChanges();
                
            }
           
        }


        // GET: Merchandises/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Merchandise merchandise = db.Merchandises.Find(id);
            if (merchandise == null)
            {
                return HttpNotFound();
            }
            return View(merchandise);
        }

        // POST: Merchandises/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Merchandise merchandise = db.Merchandises.Find(id);
            db.Merchandises.Remove(merchandise);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Merchandise/Delete/5
        [HttpDelete, ActionName("MerchDeleteConfirmed")]
        //[ValidateAntiForgeryToken]
        public void MerchDeleteConfirmed(int id)
        {
            Merchandise merchandise = db.Merchandises.Find(id);
            db.Merchandises.Remove(merchandise);
            db.SaveChanges();

            
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
