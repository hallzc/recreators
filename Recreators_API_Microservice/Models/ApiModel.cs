using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Recreators_API_Microservice.Models
{
    public partial class ApiModel : DbContext
    {
        public ApiModel()
            : base("name=ApiModel")
        {
        }

        public virtual DbSet<Adoption> Adoptions { get; set; }
        public virtual DbSet<Animal> Animals { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Cart_Merchandise> Cart_Merchandise { get; set; }
        public virtual DbSet<Login_Metrics> Login_Metrics { get; set; }
        public virtual DbSet<Merchandise> Merchandises { get; set; }
        public virtual DbSet<User_Metrics> User_Metrics { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Animal>()
                .Property(e => e.Breed)
                .IsUnicode(false);

            modelBuilder.Entity<Animal>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<Animal>()
                .Property(e => e.Registered)
                .IsUnicode(false);

            modelBuilder.Entity<Animal>()
                .Property(e => e.Color)
                .IsUnicode(false);

            modelBuilder.Entity<Cart>()
                .Property(e => e.Cart_Close_Type)
                .IsUnicode(false);

            modelBuilder.Entity<Merchandise>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Merchandise>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Merchandise>()
                .HasMany(e => e.Cart_Merchandise)
                .WithRequired(e => e.Merchandise)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.First_Name)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.Last_Name)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.Sex)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.Race)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<User_Metrics>()
                .HasMany(e => e.Carts)
                .WithRequired(e => e.User_Metrics)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User_Metrics>()
                .HasMany(e => e.Login_Metrics)
                .WithRequired(e => e.User_Metrics)
                .WillCascadeOnDelete(false);
        }
    }
}
