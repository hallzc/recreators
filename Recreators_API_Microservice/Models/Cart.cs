namespace Recreators_API_Microservice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rankup_petstoremonitor.Cart")]
    public partial class Cart
    {
        [Key]
        [Column(Order = 0)]
        public int Cart_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int User_ID { get; set; }

        public int? Qty { get; set; }

        public DateTime? Item_Added_Time { get; set; }

        public DateTime? Cart_Close_Time { get; set; }

        [StringLength(45)]
        public string Cart_Close_Type { get; set; }

        public virtual User_Metrics User_Metrics { get; set; }
    }
}
