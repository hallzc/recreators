namespace Recreators_API_Microservice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rankup_petstoremonitor.User_Metrics")]
    public partial class User_Metrics
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User_Metrics()
        {
            Adoptions = new HashSet<Adoption>();
            Carts = new HashSet<Cart>();
            Login_Metrics = new HashSet<Login_Metrics>();
        }

        [Key]
        public int User_ID { get; set; }

        [StringLength(45)]
        public string Username { get; set; }

        [StringLength(45)]
        public string First_Name { get; set; }

        [StringLength(45)]
        public string Last_Name { get; set; }

        [StringLength(45)]
        public string Email { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        public string Sex { get; set; }

        [StringLength(45)]
        public string Race { get; set; }

        [StringLength(45)]
        public string City { get; set; }

        [StringLength(45)]
        public string State { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_Of_Birth { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Adoption> Adoptions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cart> Carts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Login_Metrics> Login_Metrics { get; set; }
    }
}
