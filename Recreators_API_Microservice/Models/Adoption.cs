namespace Recreators_API_Microservice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rankup_petstoremonitor.Adoptions")]
    public partial class Adoption
    {
        [Key]
        public int Adoption_ID { get; set; }

        public int? User_ID { get; set; }

        public int? Animal_ID { get; set; }

        public DateTime? Adoption_Time { get; set; }

        public Boolean? Adoption_Complete { get; set; }

        public virtual Animal Animal { get; set; }

        public virtual User_Metrics User_Metrics { get; set; }
    }
}
