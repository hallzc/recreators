namespace Recreators_API_Microservice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("rankup_petstoremonitor.Login_Metrics")]
    public partial class Login_Metrics
    {
        [Key]
        public int Login_ID { get; set; }

        public int User_ID { get; set; }

        public DateTime? Last_Login { get; set; }

        public DateTime? Last_Logout { get; set; }

        public virtual User_Metrics User_Metrics { get; set; }
    }
}
