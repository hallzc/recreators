// <copyright file="AnimalsControllerTest.cs">Copyright ©  2021</copyright>
using System;
using System.Web.Mvc;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Recreators_API_Microservice.Controllers;
using Recreators_API_Microservice.Models;

namespace Recreators_API_Microservice.Controllers.Tests
{
    /// <summary>This class contains parameterized unit tests for AnimalsController</summary>
    [PexClass(typeof(AnimalsController))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class AnimalsControllerTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public AnimalsController ConstructorTest()
        {
            AnimalsController target = new AnimalsController();
            return target;
            // TODO: add assertions to method AnimalsControllerTest.ConstructorTest()
        }

        /// <summary>Test stub for Create()</summary>
        [PexMethod]
        public ActionResult CreateTest([PexAssumeUnderTest]AnimalsController target)
        {
            ActionResult result = target.Create();
            return result;
            // TODO: add assertions to method AnimalsControllerTest.CreateTest(AnimalsController)
        }

        /// <summary>Test stub for Create(Animal)</summary>
        [PexMethod]
        public ActionResult CreateTest01([PexAssumeUnderTest]AnimalsController target, Animal animal)
        {
            ActionResult result = target.Create(animal);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.CreateTest01(AnimalsController, Animal)
        }

        /// <summary>Test stub for DeleteConfirmed(Int32)</summary>
        [PexMethod]
        public ActionResult DeleteConfirmedTest([PexAssumeUnderTest]AnimalsController target, int id)
        {
            ActionResult result = target.DeleteConfirmed(id);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.DeleteConfirmedTest(AnimalsController, Int32)
        }

        /// <summary>Test stub for Delete(Nullable`1&lt;Int32&gt;)</summary>
        [PexMethod]
        public ActionResult DeleteTest(
            [PexAssumeUnderTest]AnimalsController target,
            int? id
        )
        {
            ActionResult result = target.Delete(id);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.DeleteTest(AnimalsController, Nullable`1<Int32>)
        }

        /// <summary>Test stub for Details(Nullable`1&lt;Int32&gt;)</summary>
        [PexMethod]
        public ActionResult DetailsTest(
            [PexAssumeUnderTest]AnimalsController target,
            int? id
        )
        {
            ActionResult result = target.Details(id);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.DetailsTest(AnimalsController, Nullable`1<Int32>)
        }

        /// <summary>Test stub for Edit(Nullable`1&lt;Int32&gt;)</summary>
        [PexMethod]
        public ActionResult EditTest(
            [PexAssumeUnderTest]AnimalsController target,
            int? id
        )
        {
            ActionResult result = target.Edit(id);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.EditTest(AnimalsController, Nullable`1<Int32>)
        }

        /// <summary>Test stub for Edit(Animal)</summary>
        [PexMethod]
        public ActionResult EditTest01([PexAssumeUnderTest]AnimalsController target, Animal animal)
        {
            ActionResult result = target.Edit(animal);
            return result;
            // TODO: add assertions to method AnimalsControllerTest.EditTest01(AnimalsController, Animal)
        }

        /// <summary>Test stub for Index()</summary>
        [PexMethod]
        public ActionResult IndexTest([PexAssumeUnderTest]AnimalsController target)
        {
            ActionResult result = target.Index();
            return result;
            // TODO: add assertions to method AnimalsControllerTest.IndexTest(AnimalsController)
        }
    }
}
